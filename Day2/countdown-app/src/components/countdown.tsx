import React, { Fragment } from 'react';

export class Countdown extends React.Component<{}, { value: number, isPaused: boolean, isStartClicked: boolean, welcome: string }> {
    screen: any;
    state = { value: 15, isPaused: false, isStartClicked: true, welcome: "" };

    start = () => {
        
        this.screen = window.setInterval(() => {
            this.setState({welcome: ""})
            if (this.state.isStartClicked) {
                if (this.state.value <= 0) {
                    clearInterval(this.screen);
                    this.setState({value: 15, welcome: "Welcome!!!"})
                    return;
                }

                this.setState((state) => {
                    return { value: state.value > 0 ? state.value - 1 : 0, isPaused: false };
                });
            }
        }, 1000);

    };

    pause = () => {
        clearInterval(this.screen);
        this.setState({ isPaused: true, isStartClicked: false });

    };

    resume = () => {
        this.setState({ isPaused: false });
        this.setState({ isStartClicked: true })
        this.start();
    };

    reset = () => {
        clearInterval(this.screen);
        this.setState({ value: 15, isPaused: false, isStartClicked: true });
    };

    componentDidMount() {
        this.start();
    }

    render() {
        const { value } = this.state;
        return (
            <Fragment>
                <div className="btn-group my-2" role="group">
                    <button type="button" className="btn btn-secondary" onClick={this.state.isStartClicked ? this.start : () => { }}>Start</button>
                    <button type="button" className="btn btn-secondary" onClick={this.state.isPaused ? this.resume : this.pause}>
                        {this.state.isPaused ? 'Resume' : 'Pause'}
                    </button>
                    <button type="button" className="btn btn-secondary" onClick={this.reset}>Reset</button>
                </div>
                <div className="d-flex align-items-center justify-content-center bg-info rounded countdown-value">
                    <h1 className="align-middle text-white font-weight-bold">{value}</h1>
                </div>
                <h3 className="mt-2">{this.state.welcome}</h3>
            </Fragment>
        );
    }
}