import React from 'react';
import './App.css';
import { NotificationSetting } from './components/notification-setting';
import { CardComponent } from './components/card-component';

function App() {
  return (
    <div className="App">
      <header className="App-header"></header>
      <NotificationSetting />
      <br />

      <CardComponent></CardComponent>
    </div>
  );
}

export default App;
