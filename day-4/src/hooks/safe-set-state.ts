import { useState, useEffect, useRef, useCallback } from "react";

export function useSafeSetState(init: any) {
    const [state, setState] = useState(init);
    const mountedRef = useRef(false);
    useEffect(() => {
        mountedRef.current = true;
        return () => {
            mountedRef.current = false;
        }
    }, []);
    const safeSetState = useCallback(function(state: any) {
        console.log(mountedRef.current)
        if (mountedRef.current) {
            setState(state);
        }
    }, []);
    
    return [state, safeSetState];
}