import React, { Component } from 'react';

interface OnlineOfflineState {
  status: 'online' | 'offline';
}

export function withNetworkStatus(OtherConponent: any) {}

export class OnlineOffline extends Component<{}, OnlineOfflineState> {
  state = {
    status: 'online' as 'offline',
  };

  updateState = (e: Event) => {
    this.setState({
      status: e.type === 'online' ? 'online' : 'offline',
    });
  };

  componentDidMount() {
    window.addEventListener('online', this.updateState);
    window.addEventListener('offline', this.updateState);
  }

  componentWillUnmount() {
    window.removeEventListener('online', this.updateState);
    window.removeEventListener('offfline', this.updateState);
  }

  render() {
    return <div>Network status: {this.state.status}</div>;
  }
}
