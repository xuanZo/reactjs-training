import React, { Component } from 'react';
import { Switch } from 'antd';

interface NotificationData {
  id: string;
  title: string;
  subTitle?: string;
  status: boolean;
}

export class NotiDetail extends Component<
  {
    noti: NotificationData;
    onChange: (checked: boolean, noti: NotificationData) => void;
  },
  {}
> {
  handleSwitchChange = (checked: boolean) => {
    this.props.onChange(checked, this.props.noti);
  };

  render() {
    const { noti } = this.props;
    return (
      <div>
        <h3> {noti.title}</h3>
        {noti ? <h4>{noti.subTitle}</h4> : null}
        <Switch checked={noti.status} onChange={this.handleSwitchChange} />
      </div>
    );
  }
}

const NotiMemo = React.memo(NotiDetail);

export class NotificationSetting extends Component<
  {},
  {
    notifications: Array<NotificationData>;
  }
> {
  state = {
    notifications: [
      {
        id: 'email',
        title: 'Email Notification',
        subTitle: 'Commits data and history',
        status: true,
      },
      {
        id: 'push',
        title: 'Push Notification',
        subTitle: 'Commits data and history',
        status: false,
      },
      {
        id: 'monthly-reports',
        title: 'Monthly Reports',
        subTitle: 'Commits data and history',
        status: false,
      },
      {
        id: 'quarter-reports',
        title: 'Quarter Reports',
        subTitle: 'Commits data and history',
        status: false,
      },
    ],
  };
  handleSwitchChange = (checked: boolean, noti: NotificationData) => {
    console.log(checked, noti);
    const notis = this.state.notifications.map((n) => {
      if (noti.id === n.id) {
        return {
          ...n,
          status: checked,
        };
      }
      return n;
    });
    this.setState({
      notifications: notis,
    });
  };
  render() {
    const { notifications } = this.state;
    return (
      <>
        {notifications.map((noti) => {
          return (
            <NotiMemo
              key={noti.id}
              noti={noti}
              onChange={this.handleSwitchChange}
            />
          );
        })}
      </>
    );
  }
}
