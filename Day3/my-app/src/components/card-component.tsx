import React, { Component } from 'react';
import { Card, Row, Col } from 'antd';
import { CheckCircleFilled, EnvironmentOutlined } from '@ant-design/icons';

interface CardData {
  id: string;
  type: 'location' | 'tick';
  distance: number;
  price: number;
  description?: string;
}

export class CardDetail extends Component<
  {
    card: CardData;
  },
  {}
> {
  render() {
    const { card } = this.props;
    return (
      <Col span={12}>
        <Card>
          {card.type === 'location' ? (
            <EnvironmentOutlined style={{ fontSize: 32, color: '#b7eb8f' }} />
          ) : (
            <CheckCircleFilled style={{ fontSize: 32, color: '#b7eb8f' }} />
          )}
          {card.type === 'location' ? (
            <p className="card-text">{card.distance.toLocaleString('en')} mi</p>
          ) : (
            <p className="card-text">${card.price.toLocaleString('en')}</p>
          )}
          <p>{card.description}</p>
        </Card>
      </Col>
    );
  }
}

const CardDetailMemo = React.memo(CardDetail);

export class CardComponent extends Component<
  {},
  {
    arrCard: Array<CardData>;
  }
> {
  state = {
    arrCard: [
      {
        id: '01',
        type: 'location' as 'location',
        distance: 158.3,
        price: 0,
        description: 'Distance driven',
      },
      {
        id: '02',
        type: 'tick' as 'tick',
        distance: 0,
        price: 1428,
        description: 'Vehicles on track',
      },
    ],
  };
  componentDidMount() {
    window.setInterval(() => {
      const newCards = this.state.arrCard.map((card) => {
        return {
          ...card,
          price: Math.floor(Math.random() * 10000),
          distance: Math.random() * 1000,
        };
      });
      this.setState({
        arrCard: newCards,
      });
    }, 5000);
  }
  render() {
    const { arrCard } = this.state;
    return (
      <>
        <div className="site-card-wrapper">
          <Row gutter={16} align="middle">
            {arrCard.map((card) => {
              return <CardDetailMemo key={card.id} card={card} />;
            })}
          </Row>
        </div>
      </>
    );
  }
}
