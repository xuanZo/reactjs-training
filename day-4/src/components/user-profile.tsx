import React, { useEffect } from "react";
import { useSafeSetState } from "../hooks/safe-set-state";


export function UserProfile() {
    const [username, setUsername] = useSafeSetState('Tiep');
    useEffect(() => {
        setTimeout(() => {
            setUsername('unmounted')
        }, 5000);
    }, [setUsername]);
    function onChange(e: React.ChangeEvent<HTMLInputElement>) {
        const value = e.target.value;
        setUsername(value);
    }
    return (
        <div>
            {username}
            <input type="text" value={username} onChange={onChange}/>
        </div>
    );
}
