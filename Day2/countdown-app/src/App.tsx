import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Countdown } from './components/countdown';
import { RatingBar } from './components/rating';
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h2 className="mt-3">Countdown component</h2>
          <Countdown></Countdown>
          <hr className="my-5" />
          <h2 className="mt-3">Rating component</h2>
          <RatingBar max={10} ratingValue={8} />
        </header>
      </div>
    );
  }
}

export default App;
