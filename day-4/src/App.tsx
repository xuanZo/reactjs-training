import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { User } from './models/user';
import { Header } from './components/header';
import { SideNav } from './components/sidenav';
import { UCProvider } from './contexts/user-context';
import { UserProfile } from './components/user-profile';
import { RatingBar } from './components/rating-bar';
import { NetworkStatus } from './components/online-offline';
interface AppState {
  user: User;
  theme?: 'light' | 'dark';
  lang?: 'vi' | 'en-US';
  showProfile: boolean;
}

class App extends Component<{}, AppState> {
  state: AppState = {
    user: {
      id: 'some-uuid',
      avatarUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Mr._Smiley_Face.svg/240px-Mr._Smiley_Face.svg.png',
      playlist: ['Microservices', 'TypeScript programming'],
      username: 'John Doe',
      searchHistory: [],
    },
    showProfile: true
  };

  render() {
    const {user, showProfile} = this.state;
    return (
      <UCProvider value={user}>
        <div className="container">
          <Header/>
          <main>
            main content

            <div>
              username {user.username}
            </div>
            {showProfile && <UserProfile /> }
            <button onClick={() => this.setState((pre) => {
              return {showProfile: !pre.showProfile}
            })}>Toggle</button>
            <RatingBar max={10} ratingValue={8} />
            <NetworkStatus />
          </main>
          <aside>
            <SideNav user={user}/>
          </aside>
          <hr/>
        </div>
      </UCProvider>
    );
  }
}

export default App;
