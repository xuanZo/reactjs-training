import React from "react";
import { useNetwork } from "../hooks/network";

export function NetworkStatus() {
    const networkStatus = useNetwork();
    const className = "ns-" + networkStatus;
    return (
      <div className="ns-container">
        <span className={className}></span>
        {networkStatus}
      </div>
    );
  }